package ru.t1.malyugin.tm.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.malyugin.tm.dto.response.user.UserGetProfileResponse;
import ru.t1.malyugin.tm.dto.response.user.UserLoginResponse;
import ru.t1.malyugin.tm.dto.response.user.UserLogoutResponse;

public interface IAuthEndpointClient extends IEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserGetProfileResponse getProfile(@NotNull UserGetProfileRequest request);

}