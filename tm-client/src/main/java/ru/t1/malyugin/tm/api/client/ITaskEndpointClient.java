package ru.t1.malyugin.tm.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.*;
import ru.t1.malyugin.tm.dto.response.task.*;

public interface ITaskEndpointClient extends IEndpointClient {

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskCreateResponse creteTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskShowListResponse showTaskList(@NotNull TaskShowListRequest request);

    @NotNull
    TaskShowListByProjectIdResponse showTaskListByProject(@NotNull TaskShowListByProjectIdRequest request);

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindToProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}