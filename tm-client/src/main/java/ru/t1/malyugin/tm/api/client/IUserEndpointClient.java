package ru.t1.malyugin.tm.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.dto.response.user.*;

public interface IUserEndpointClient extends IEndpointClient {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request);

}