package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLogDir();

    @NotNull
    String getApplicationCommandScannerDir();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

}