package ru.t1.malyugin.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.client.IEndpointClient;
import ru.t1.malyugin.tm.dto.response.AbstractResultResponse;
import ru.t1.malyugin.tm.exception.server.ConnectionException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host;

    @NotNull
    private Integer port;

    @Nullable
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.getHost();
        this.port = client.getPort();
        this.socket = client.getSocket();
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(
            @Nullable final Object data,
            @NotNull final Class<T> clazz
    ) {
        if (socket == null) throw new ConnectionException();
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof AbstractResultResponse && !((AbstractResultResponse) result).getSuccess()) {
            @NotNull final AbstractResultResponse response = (AbstractResultResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    @Override
    public void disconnect() throws IOException {
        if (socket == null) return;
        socket.close();
    }

}