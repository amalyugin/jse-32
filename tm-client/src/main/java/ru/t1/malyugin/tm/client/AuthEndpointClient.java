package ru.t1.malyugin.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.client.IAuthEndpointClient;
import ru.t1.malyugin.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.malyugin.tm.dto.response.user.UserGetProfileResponse;
import ru.t1.malyugin.tm.dto.response.user.UserLoginResponse;
import ru.t1.malyugin.tm.dto.response.user.UserLogoutResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserGetProfileResponse getProfile(@NotNull final UserGetProfileRequest request) {
        return call(request, UserGetProfileResponse.class);
    }

}