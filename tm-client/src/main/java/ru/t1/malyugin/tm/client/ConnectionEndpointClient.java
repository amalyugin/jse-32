package ru.t1.malyugin.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.IPropertyService;

public class ConnectionEndpointClient extends AbstractEndpointClient {

    public ConnectionEndpointClient(@NotNull final IPropertyService propertyService) {
        super();
        this.setHost(propertyService.getServerHost());
        this.setPort(propertyService.getServerPort());
    }

}