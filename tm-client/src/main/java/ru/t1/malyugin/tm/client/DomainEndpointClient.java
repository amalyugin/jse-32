package ru.t1.malyugin.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.client.IDomainEndpointClient;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.response.data.load.*;
import ru.t1.malyugin.tm.dto.response.data.save.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonFasterXmlLoadResponse loadJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJsonFasterXmlSaveResponse saveJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonJaxBLoadResponse loadJsonJaxB(@NotNull final DataJsonJaxBLoadRequest request) {
        return call(request, DataJsonJaxBLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJsonJaxBSaveResponse saveJsonJaxB(@NotNull final DataJsonJaxBSaveRequest request) {
        return call(request, DataJsonJaxBSaveResponse.class);
    }

    @NotNull
    @Override
    public DataXmlFasterXmlLoadResponse loadXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    public DataXmlFasterXmlSaveResponse saveXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    public DataXmlJaxBLoadResponse loadXmlJaxB(@NotNull final DataXmlJaxBLoadRequest request) {
        return call(request, DataXmlJaxBLoadResponse.class);
    }

    @NotNull
    @Override
    public DataXmlJaxBSaveResponse saveXmlJaxB(@NotNull final DataXmlJaxBSaveRequest request) {
        return call(request, DataXmlJaxBSaveResponse.class);
    }

    @NotNull
    @Override
    public DataYamlFasterXmlLoadResponse loadYaml(@NotNull final DataYamlFasterXmlLoadRequest request) {
        return call(request, DataYamlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    public DataYamlFasterXmlSaveResponse saveYaml(@NotNull final DataYamlFasterXmlSaveRequest request) {
        return call(request, DataYamlFasterXmlSaveResponse.class);
    }

}