package ru.t1.malyugin.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.client.ISystemEndpointClient;
import ru.t1.malyugin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerInfoRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.malyugin.tm.dto.response.system.ServerAboutResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerInfoResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    public SystemEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

    @NotNull
    @Override
    public ServerInfoResponse getInfo(@NotNull final ServerInfoRequest request) {
        return call(request, ServerInfoResponse.class);
    }

}