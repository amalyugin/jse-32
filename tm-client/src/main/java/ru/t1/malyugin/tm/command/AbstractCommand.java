package ru.t1.malyugin.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.model.ICommand;
import ru.t1.malyugin.tm.api.service.IEndpointLocator;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @Getter
    @Setter
    @NotNull
    private IServiceLocator serviceLocator;

    @Getter
    @Setter
    @NotNull
    private IEndpointLocator endpointLocator;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract Role[] getRoles();

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();

        boolean isName = !StringUtils.isBlank(name);
        boolean isArgument = !StringUtils.isBlank(argument);
        boolean isDescription = !StringUtils.isBlank(description);

        result += (isName ? name + (isArgument ? ", " : "") : "");
        result += (isArgument ? argument : "");
        result += (isDescription ? " -> " + description : "");

        return result;
    }

}