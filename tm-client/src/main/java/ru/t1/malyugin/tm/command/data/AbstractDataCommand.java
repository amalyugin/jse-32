package ru.t1.malyugin.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.client.IDomainEndpointClient;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpointClient getDomainEndpoint() {
        return getEndpointLocator().getDomainEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}