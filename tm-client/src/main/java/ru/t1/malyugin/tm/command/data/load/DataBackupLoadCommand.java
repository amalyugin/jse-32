package ru.t1.malyugin.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.load.DataBackupLoadRequest;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-backup";

    @NotNull
    private static final String DESCRIPTION = "Load backup from binary file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest();
        getDomainEndpoint().loadBackup(request);
    }

}