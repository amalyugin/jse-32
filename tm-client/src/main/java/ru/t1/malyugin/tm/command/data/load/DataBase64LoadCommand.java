package ru.t1.malyugin.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.load.DataBase64LoadRequest;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    private static final String DESCRIPTION = "Load data from base64 file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest();
        getDomainEndpoint().loadBase64(request);
    }

}