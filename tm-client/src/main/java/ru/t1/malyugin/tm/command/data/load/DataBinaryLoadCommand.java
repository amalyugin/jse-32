package ru.t1.malyugin.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.load.DataBinaryLoadRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-binary";

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest();
        getDomainEndpoint().loadBinary(request);
    }

}