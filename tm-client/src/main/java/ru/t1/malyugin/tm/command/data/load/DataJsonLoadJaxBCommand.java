package ru.t1.malyugin.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.load.DataJsonJaxBLoadRequest;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from JSON (JaxB)";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataJsonJaxBLoadRequest request = new DataJsonJaxBLoadRequest();
        getDomainEndpoint().loadJsonJaxB(request);
    }

}