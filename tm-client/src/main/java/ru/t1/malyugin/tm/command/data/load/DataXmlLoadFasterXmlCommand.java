package ru.t1.malyugin.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.load.DataXmlFasterXmlLoadRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    private static final String DESCRIPTION = "Load data from XML (FasterXML)";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataXmlFasterXmlLoadRequest request = new DataXmlFasterXmlLoadRequest();
        getDomainEndpoint().loadXmlFasterXml(request);
    }

}