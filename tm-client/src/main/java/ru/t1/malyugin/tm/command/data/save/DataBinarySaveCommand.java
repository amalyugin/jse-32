package ru.t1.malyugin.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.save.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-binary";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        getDomainEndpoint().saveBinary(request);
    }

}