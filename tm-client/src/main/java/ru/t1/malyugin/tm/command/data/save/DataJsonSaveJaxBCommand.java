package ru.t1.malyugin.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.save.DataJsonJaxBSaveRequest;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data in JSON (JaxB)";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataJsonJaxBSaveRequest request = new DataJsonJaxBSaveRequest();
        getDomainEndpoint().saveJsonJaxB(request);
    }

}