package ru.t1.malyugin.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.data.AbstractDataCommand;
import ru.t1.malyugin.tm.dto.request.data.save.DataXmlFasterXmlSaveRequest;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    private static final String DESCRIPTION = "Save data in XML (FasterXML)";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final DataXmlFasterXmlSaveRequest request = new DataXmlFasterXmlSaveRequest();
        getDomainEndpoint().saveXmlFasterXml(request);
    }

}