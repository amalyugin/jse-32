package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start project by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");

        System.out.print("ENTER PROJECT INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;

        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(index);
        getProjectEndpoint().startProjectByIndex(request);
    }

}