package ru.t1.malyugin.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.client.IEndpointClient;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull
    private static final String NAME = "connect";

    @NotNull
    private static final String DESCRIPTION = "Connect to server";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            @NotNull final IEndpointClient endpointClient = getEndpointLocator().getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();

            getEndpointLocator().getAuthEndpointClient().setSocket(socket);
            getEndpointLocator().getSystemEndpointClient().setSocket(socket);
            getEndpointLocator().getDomainEndpointClient().setSocket(socket);
            getEndpointLocator().getProjectEndpointClient().setSocket(socket);
            getEndpointLocator().getTaskEndpointClient().setSocket(socket);
            getEndpointLocator().getUserEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[0];
    }
}
