package ru.t1.malyugin.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    private static final String NAME = "disconnect";

    @NotNull
    private static final String DESCRIPTION = "Disconnect from server";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getEndpointLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[0];
    }
}
