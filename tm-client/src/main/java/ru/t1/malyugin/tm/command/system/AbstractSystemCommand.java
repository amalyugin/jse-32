package ru.t1.malyugin.tm.command.system;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.client.ISystemEndpointClient;
import ru.t1.malyugin.tm.api.service.ICommandService;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    protected ISystemEndpointClient getSystemEndpoint() {
        return getEndpointLocator().getSystemEndpointClient();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

}