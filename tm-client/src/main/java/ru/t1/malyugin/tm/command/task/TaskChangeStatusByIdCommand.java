package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-change-status-by-id";

    @NotNull
    private static final String DESCRIPTION = "Change task status by id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");

        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        @NotNull final Status status = Status.getStatusByIndex(statusIndex);

        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(id, status);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}