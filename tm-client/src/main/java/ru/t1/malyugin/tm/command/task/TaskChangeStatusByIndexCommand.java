package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-change-status-by-index";

    @NotNull
    private static final String DESCRIPTION = "Change task status by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");

        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer taskIndex = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        @NotNull final Status status = Status.getStatusByIndex(statusIndex);

        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(taskIndex, status);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

}