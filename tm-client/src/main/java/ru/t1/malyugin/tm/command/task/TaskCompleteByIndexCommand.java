package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-complete-by-index";

    @NotNull
    private static final String DESCRIPTION = "Complete task by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");

        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;

        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(index);
        getTaskEndpoint().completeTaskByIndex(request);
    }

}