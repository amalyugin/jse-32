package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.task.TaskShowListRequest;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-list";

    @NotNull
    private static final String DESCRIPTION = "Show task list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK LIST]");

        System.out.print("ENTER SORT: ");
        System.out.print(Sort.renderValuesList());
        @Nullable final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        @Nullable final Sort sort = Sort.getSortByIndex(sortIndex);

        @NotNull final TaskShowListRequest request = new TaskShowListRequest(sort);
        @Nullable final List<Task> tasks = getTaskEndpoint().showTaskList(request).getTaskList();
        renderTaskList(tasks);
    }

}