package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "Remove task by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");

        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;

        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

}