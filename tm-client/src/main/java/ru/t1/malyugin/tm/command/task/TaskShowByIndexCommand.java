package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.task.TaskShowByIndexRequest;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Show task by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");

        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;

        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(index);
        @Nullable final Task task = getTaskEndpoint().showTaskByIndex(request).getTask();
        renderTask(task);
    }

}