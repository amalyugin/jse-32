package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-update-by-index";

    @NotNull
    private static final String DESCRIPTION = "Update task by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER NEW NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

}