package ru.t1.malyugin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-change-password";

    @NotNull
    private static final String DESCRIPTION = "change user password";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");

        System.out.print("ENTER NEW PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(password);
        getUserEndpoint().changePassword(request);
    }

}