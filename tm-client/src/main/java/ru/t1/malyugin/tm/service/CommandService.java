package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.api.service.ICommandService;
import ru.t1.malyugin.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private @NotNull
    final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (StringUtils.isBlank(argument)) return null;
        return commandRepository.getCommandByArgument(argument.trim());
    }

    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (StringUtils.isBlank(name)) return null;
        return commandRepository.getCommandByName(name.trim());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}