package ru.t1.malyugin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.response.data.load.*;
import ru.t1.malyugin.tm.dto.response.data.save.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonFasterXmlLoadResponse loadJsonFasterXml(@NotNull DataJsonFasterXmlLoadRequest request);

    @NotNull
    DataJsonFasterXmlSaveResponse saveJsonFasterXml(@NotNull DataJsonFasterXmlSaveRequest request);

    @NotNull
    DataJsonJaxBLoadResponse loadJsonJaxB(@NotNull DataJsonJaxBLoadRequest request);

    @NotNull
    DataJsonJaxBSaveResponse saveJsonJaxB(@NotNull DataJsonJaxBSaveRequest request);

    @NotNull
    DataXmlFasterXmlLoadResponse loadXmlFasterXml(@NotNull DataXmlFasterXmlLoadRequest request);

    @NotNull
    DataXmlFasterXmlSaveResponse saveXmlFasterXml(@NotNull DataXmlFasterXmlSaveRequest request);

    @NotNull
    DataXmlJaxBLoadResponse loadXmlJaxB(@NotNull DataXmlJaxBLoadRequest request);

    @NotNull
    DataXmlJaxBSaveResponse saveXmlJaxB(@NotNull DataXmlJaxBSaveRequest request);

    @NotNull
    DataYamlFasterXmlLoadResponse loadYaml(@NotNull DataYamlFasterXmlLoadRequest request);

    @NotNull
    DataYamlFasterXmlSaveResponse saveYaml(@NotNull DataYamlFasterXmlSaveRequest request);

}