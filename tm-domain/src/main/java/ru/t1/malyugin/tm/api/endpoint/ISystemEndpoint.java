package ru.t1.malyugin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerInfoRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.malyugin.tm.dto.response.system.ServerAboutResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerInfoResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

    @NotNull
    ServerInfoResponse getInfo(@NotNull ServerInfoRequest request);

}