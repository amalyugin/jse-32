package ru.t1.malyugin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request);

}