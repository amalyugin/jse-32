package ru.t1.malyugin.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer projectIndex;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final Integer projectIndex, @Nullable final Status status) {
        this.projectIndex = projectIndex;
        this.status = status;
    }

}