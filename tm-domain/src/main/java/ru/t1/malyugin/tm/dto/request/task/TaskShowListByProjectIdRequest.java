package ru.t1.malyugin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowListByProjectIdRequest(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}