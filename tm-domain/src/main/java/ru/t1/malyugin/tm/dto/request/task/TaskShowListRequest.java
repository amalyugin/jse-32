package ru.t1.malyugin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskShowListRequest(@Nullable final Sort sort) {
        this.sort = sort;
    }

}