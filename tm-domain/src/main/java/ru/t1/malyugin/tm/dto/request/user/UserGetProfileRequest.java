package ru.t1.malyugin.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserGetProfileRequest extends AbstractUserRequest {

}