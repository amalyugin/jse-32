package ru.t1.malyugin.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;
import ru.t1.malyugin.tm.model.Project;


@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIndexResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public ProjectCompleteByIndexResponse(@Nullable final Project project) {
        this.project = project;
    }

}