package ru.t1.malyugin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResultResponse;
import ru.t1.malyugin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private User user;

    public UserLoginResponse(@Nullable final User user) {
        this.user = user;
    }

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}