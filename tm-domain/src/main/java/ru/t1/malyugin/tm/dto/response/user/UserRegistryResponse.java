package ru.t1.malyugin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;
import ru.t1.malyugin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserRegistryResponse extends AbstractResponse {

    @Nullable
    private User user;

    public UserRegistryResponse(@Nullable final User user) {
        this.user = user;
    }

}