package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(
            @Nullable String userId
    );

    int getSize(
            @Nullable String userId
    );

    @NotNull
    M removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    M removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    List<M> findAll(
            @Nullable String userId
    );

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Comparator<M> comparator
    );

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

}