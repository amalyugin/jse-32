package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    );

    @Nullable
    User findOneByLogin(
            @Nullable String login
    );

    @Nullable
    User findOneByEmail(
            @Nullable String email
    );

    User removeByLogin(
            @Nullable String login
    );

    @NotNull
    User removeByEmail(
            @Nullable String email
    );

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(
            @Nullable String login
    );

    @NotNull
    Boolean isEmailExist(
            @Nullable String email
    );

    User lockUserByLogin(
            @Nullable String login
    );

    User unlockUserByLogin(
            @Nullable String login
    );

}