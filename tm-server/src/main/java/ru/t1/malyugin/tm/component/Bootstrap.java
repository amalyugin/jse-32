package ru.t1.malyugin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.*;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.request.project.*;
import ru.t1.malyugin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerInfoRequest;
import ru.t1.malyugin.tm.dto.request.system.ServerVersionRequest;
import ru.t1.malyugin.tm.dto.request.task.*;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.endpoint.*;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;
import ru.t1.malyugin.tm.repository.UserRepository;
import ru.t1.malyugin.tm.service.*;
import ru.t1.malyugin.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerInfoRequest.class, systemEndpoint::getInfo);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveBinary);
        server.registry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::loadJsonFasterXml);
        server.registry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::saveJsonFasterXml);
        server.registry(DataJsonJaxBLoadRequest.class, domainEndpoint::loadJsonJaxB);
        server.registry(DataJsonJaxBSaveRequest.class, domainEndpoint::saveJsonJaxB);
        server.registry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::loadXmlFasterXml);
        server.registry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::saveXmlFasterXml);
        server.registry(DataXmlJaxBLoadRequest.class, domainEndpoint::loadXmlJaxB);
        server.registry(DataXmlJaxBSaveRequest.class, domainEndpoint::saveXmlJaxB);
        server.registry(DataYamlFasterXmlLoadRequest.class, domainEndpoint::loadYaml);
        server.registry(DataYamlFasterXmlSaveRequest.class, domainEndpoint::saveYaml);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfile);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectByIndex);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectById);
        server.registry(ProjectCreateRequest.class, projectEndpoint::creteProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectShowListRequest.class, projectEndpoint::showProjectList);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskByIndex);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskById);
        server.registry(TaskCreateRequest.class, taskEndpoint::creteTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskShowListRequest.class, taskEndpoint::showTaskList);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskShowListByProjectIdRequest.class, taskEndpoint::showTaskListByProject);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User user = userService.create("user", "user", "user@m.ru");
        @NotNull final User admin = userService.create("admin", "admin", "admin@m.ru", Role.ADMIN);
        userService.create("test", "test", "test@m.ru");

        @NotNull final Project project1 = projectService.create(user.getId(), "P1", "D1");
        @NotNull final Project project2 = projectService.create(user.getId(), "P2", "D2");
        @NotNull final Project project3 = projectService.create(admin.getId(), "P3", "D3");

        @NotNull final Task task1 = taskService.create(user.getId(), "T1", "T1");
        @NotNull final Task task2 = taskService.create(user.getId(), "T2", "T2");
        @NotNull final Task task3 = taskService.create(user.getId(), "T3", "T3");
        @NotNull final Task task4 = taskService.create(user.getId(), "T4", "T4");
        @NotNull final Task task5 = taskService.create(admin.getId(), "T5", "T5");
        @NotNull final Task task6 = taskService.create(admin.getId(), "T6", "T6");

        projectTaskService.bindTaskToProject(user.getId(), project1.getId(), task2.getId());
        projectTaskService.bindTaskToProject(user.getId(), project1.getId(), task3.getId());
        projectTaskService.bindTaskToProject(user.getId(), project2.getId(), task4.getId());
        projectTaskService.bindTaskToProject(user.getId(), project2.getId(), task1.getId());
        projectTaskService.bindTaskToProject(admin.getId(), project3.getId(), task6.getId());
        projectTaskService.bindTaskToProject(admin.getId(), project3.getId(), task5.getId());
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TM SERVER **");
    }

    private void initLog4j() {
        BasicConfigurator.configure();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        initLogger();
        initLog4j();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TM SERVER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}