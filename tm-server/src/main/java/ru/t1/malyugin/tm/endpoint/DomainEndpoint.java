package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.malyugin.tm.api.service.IDomainService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.response.data.load.*;
import ru.t1.malyugin.tm.dto.response.data.save.*;
import ru.t1.malyugin.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadBackup(@NotNull final DataBackupLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveBackup(@NotNull final DataBackupSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64(@NotNull final DataBase64LoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64(@NotNull final DataBase64SaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinary(@NotNull final DataBinaryLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinary(@NotNull final DataBinarySaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXmlLoadResponse loadJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXmlSaveResponse saveJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonJaxBLoadResponse loadJsonJaxB(@NotNull final DataJsonJaxBLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataJsonJaxBLoadResponse();
    }

    @NotNull
    @Override
    public DataJsonJaxBSaveResponse saveJsonJaxB(@NotNull final DataJsonJaxBSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataJsonJaxBSaveResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXmlLoadResponse loadXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXmlSaveResponse saveXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    public DataXmlJaxBLoadResponse loadXmlJaxB(@NotNull final DataXmlJaxBLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataXmlJaxBLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlJaxBSaveResponse saveXmlJaxB(@NotNull final DataXmlJaxBSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataXmlJaxBSaveResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXmlLoadResponse loadYaml(@NotNull final DataYamlFasterXmlLoadRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXmlSaveResponse saveYaml(@NotNull final DataYamlFasterXmlSaveRequest request) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }

}