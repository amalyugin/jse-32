package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.IUserEndpoint;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.dto.request.user.*;
import ru.t1.malyugin.tm.dto.response.user.*;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateProfile(@NotNull final UserUpdateProfileRequest request) {
        checkPermission(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}