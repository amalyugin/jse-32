package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.service.IAuthService;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.UserAuthException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AuthService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    private IUserService getUserService() {
        return serviceLocator.getUserService();
    }


    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return getUserService().create(login, password, email);
    }

    @Override
    public User checkCredentials(@Nullable final String login, @Nullable final String password) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        @Nullable final User user = getUserService().findOneByLogin(login.trim());
        if (user == null) throw new UserAuthException();
        final boolean locked = user.getLocked();
        if (locked) throw new UserAuthException();
        @Nullable final String passwordHash = HashUtil.salt(getPropertyService(), password);
        if (!StringUtils.equals(passwordHash, user.getPasswordHash())) throw new UserAuthException();
        return user;
    }

}