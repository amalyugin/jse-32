package ru.t1.malyugin.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.service.IAuthService;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.component.Server;
import ru.t1.malyugin.tm.dto.request.AbstractRequest;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLoginRequest;
import ru.t1.malyugin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;
import ru.t1.malyugin.tm.dto.response.system.ServerErrorResponse;
import ru.t1.malyugin.tm.dto.response.user.UserGetProfileResponse;
import ru.t1.malyugin.tm.dto.response.user.UserLoginResponse;
import ru.t1.malyugin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.User;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractRequest request;

    @Nullable
    private AbstractResponse response;

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server, socket);
    }

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket,
            @Nullable final String userId
    ) {
        super(server, socket, userId);
    }

    @Override
    @SneakyThrows
    public void run() {
        processInput();

        processUserId();
        processLogin();
        processProfile();
        processLogout();

        processOperation();
        processOutput();
    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    @SneakyThrows
    private void processInput() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    private void processLogin() {
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try {
            @NotNull final UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @Nullable final String login = userLoginRequest.getLogin();
            @Nullable final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getServiceLocator().getAuthService();
            @NotNull final User user = authService.checkCredentials(login, password);
            userId = user.getId();
            response = new UserLoginResponse(user);
        } catch (Exception e) {
            response = new UserLoginResponse(e);
        }
    }

    private void processProfile() {
        if (response != null) return;
        if (!(request instanceof UserGetProfileRequest)) return;
        if (userId == null) {
            response = new UserGetProfileResponse();
            return;
        }
        @NotNull final IUserService userService = server.getServiceLocator().getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        response = new UserGetProfileResponse(user);
    }

    private void processLogout() {
        if (response != null) return;
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
        response = new UserLogoutResponse();
    }

    private void processOperation() {
        if (response != null) return;
        try {
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (Exception e) {
            response = new ServerErrorResponse(e);
        }
    }

    @SneakyThrows
    private void processOutput() {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

}